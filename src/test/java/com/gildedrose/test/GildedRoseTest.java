package com.gildedrose.test;

import org.junit.jupiter.api.*;
import static org.junit.jupiter.api.Assertions.*;

import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;
import com.gildedrose.*;
import com.gildedrose.items.*;

class GildedRoseTest {

    InventoryInteractor interactor = new InventoryInteractor(new InMemoryInventoryRepository());
    List<String> autorizedItem = new ArrayList<>();
    Shop shop = new Shop(interactor,autorizedItem);

    GildedRoseTest() throws FileNotFoundException {
    }

    @Test
    void values_decrease_every_day () {

        interactor.UpdateQuality();
        Item items =  interactor.GetInventory().get(0);

        assertEquals(14,items.sellIn);
        assertEquals(17,items.quality);
    }

    @Test
    void quality_never_negative(){
        interactor.UpdateQuality();
        Item itemZero = interactor.GetInventory().get(1);

        assertEquals(0,itemZero.quality);

    }

    @Test
    void Aged_improve_every_day(){
        interactor.UpdateQuality();
        Item AgedItem = interactor.GetInventory().get(2);

        assertEquals(16,AgedItem.quality);
    }

    @Test
    void quality_never_over_fifty(){
        interactor.UpdateQuality();
        Item item = interactor.GetInventory().get(3);

        assertEquals(50,item.quality);

    }

    @Test
    void Legendary_item(){
        interactor.UpdateQuality();
        Item legendaryItem = interactor.GetInventory().get(4);

        assertNull(legendaryItem.sellIn);
        assertEquals(80,legendaryItem.quality);
    }

    @Test
    void Backstage_item(){
        interactor.UpdateQuality();
        Item backstageItem = interactor.GetInventory().get(5);

        assertEquals(18,backstageItem.quality);
    }


    @Test
    void Conjured_item_decrease_by_two(){
        interactor.UpdateQuality();
        Item conjuredItem = interactor.GetInventory().get(6);

        assertEquals(28,conjuredItem.quality);
    }

    @Test
    void buy_item_autorized(){

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");

        shop.buyItem(interactor.GetInventory().get(1));

        assertEquals(-55,shop.balance);
        assertEquals(9,interactor.GetInventory().size());
    }

    @Test
    void sell_item_autorized(){

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");

        shop.sellItem(interactor.GetInventory().get(1));

        assertEquals(55,shop.balance);
        assertEquals(7,interactor.GetInventory().size());
    }

    @Test
    void buy_item_non_autorized(){

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");


        assertEquals("objet non autorisé", shop.buyItem(interactor.GetInventory().get(5)));

    }

    @Test
    void sell_item_non_autorized(){

        autorizedItem.add("class com.gildedrose.items.GenericItem");
        autorizedItem.add("class com.gildedrose.items.AgedItem");


        assertEquals("objet non autorisé à la vente", shop.sellItem(interactor.GetInventory().get(5)));

    }

    @Test
    void relic_Item_not_salable(){

        autorizedItem.add("class com.gildedrose.items.RelicItem");

        assertEquals("objet non autorisé à la vente", shop.sellItem(interactor.GetInventory().get(7)));

    }

    @Test
    void balance_update_with_relic_item (){

        autorizedItem.add("class com.gildedrose.items.RelicItem");
        shop.buyItem(interactor.GetInventory().get(7));
        shop.add_relic_value();



        assertEquals(-9.8,Math.floor(shop.balance*100)/100);
    }

    @Test
    void transmuter_item(){

        Transmuter transmute_item = new Transmuter();
        Item item = interactor.GetInventory().get(3);

        transmute_item.transmuter(shop,item);

        assertEquals(shop.balance,100);
        assertEquals(40,item.quality);

    }

    @Test
    void transmuter_relic(){

        Transmuter transmute_item = new Transmuter();
        RelicItem relic = (RelicItem) interactor.GetInventory().get(7);

        transmute_item.transmuter(shop,relic);

        assertEquals(shop.balance,100);
        assertEquals(0,relic.quality);

    }

    /*@Test
    void failed_test(){
        assertTrue(false);
    }*/


}

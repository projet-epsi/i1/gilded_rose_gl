package com.gildedrose.interfaces;

import com.gildedrose.Item;

import java.util.List;

public interface InventoryViewer {
    List<Item> GetInventory();
}

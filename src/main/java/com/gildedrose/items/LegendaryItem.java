package com.gildedrose.items;

import com.gildedrose.Item;

public class LegendaryItem extends Item {


    public LegendaryItem(String name, Integer sellIn, Integer quality, float value) {
        super(name, sellIn, quality, value);
        checkSellIn(sellIn);
    }

    @Override
    public void updateQuality() {

    }

    protected void checkSellIn(Integer sellIn){
        this.sellIn = this.sellIn != null ? null : this.sellIn;
    }

}

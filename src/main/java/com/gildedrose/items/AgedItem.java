package com.gildedrose.items;

import com.gildedrose.Item;

public class AgedItem extends Item {

    public AgedItem(String name, Integer sellIn, Integer quality, float value) {
        super(name, sellIn, quality, value);
    }

    @Override
    public void updateQuality() {
        this.sellIn--;
        this.quality++;
        this.ceilQualityToFifty();
        this.floorQualityToZero();
    }
}

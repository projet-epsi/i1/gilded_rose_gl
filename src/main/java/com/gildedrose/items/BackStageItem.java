package com.gildedrose.items;

import com.gildedrose.Item;

public class BackStageItem extends Item {


    public BackStageItem(String name, Integer sellIn, Integer quality, float value) {
        super(name, sellIn, quality, value);
    }

    @Override
    public void updateQuality() {



       if(this.sellIn < 0){
           this.quality = 0;
        }
       else if(this.sellIn <= 5){
            this.quality = this.quality +3;
        }
       else if(this.sellIn <= 10){
           this.quality = this.quality +2;
       }
        else if(this.sellIn > 10){
            this.quality++;
        }


        this.ceilQualityToFifty();
        this.floorQualityToZero();

        this.sellIn--;
    }

}

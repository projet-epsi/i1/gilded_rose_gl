package com.gildedrose;

import java.io.*;
import java.util.List;

public class CsvFunctions {

    public void csvWriter(List<Item> items) throws FileNotFoundException {
        File csvFile = new File("item_list.csv");
        PrintWriter out = new PrintWriter(csvFile);
        for(Item i : items){
            out.printf(String.valueOf(i));
        }
        out.close();
    }

    public void csvReader() throws IOException {
        String path = "D:\\architecture\\gilded_rose_gl\\item_list.csv";
        try {
            BufferedReader br = new BufferedReader(new FileReader(path));
            String line = "";
            while((line = br.readLine())!= null){
                String[] values = line.split(",");
                System.out.println(values);

            }
        }
        catch (FileNotFoundException e){
            e.printStackTrace();
        } catch (IOException e){
            e.printStackTrace();
        }

    }
}

package com.gildedrose;

import com.gildedrose.interfaces.InventoryRepository;
import com.gildedrose.interfaces.InventoryUpdater;
import com.gildedrose.interfaces.InventoryViewer;

import java.util.List;

public class InventoryInteractor implements InventoryUpdater, InventoryViewer {

        private InventoryRepository repository;

        public InventoryInteractor(InventoryRepository repository){
            this.repository = repository;
        }

        public void UpdateQuality(){
            List<Item> items = this.repository.getInventory();

            for(Item item:items){
                item.updateQuality();
            }

            this.repository.saveInventory(items);
        }

        public List<Item> GetInventory(){
            return this.repository.getInventory();
        }
}

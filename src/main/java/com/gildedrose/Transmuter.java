package com.gildedrose;

import com.gildedrose.Shop;
import com.gildedrose.Item;
import com.gildedrose.items.RelicItem;

public class Transmuter {

    private Shop shop;
    private Item item;
    private RelicItem relic;

    public void transmuter(Shop shop, Item item) {
        this.item = item;
        this.item.quality -= 10;
        if(this.item.quality < 0){
            this.item.quality = 0;
        }

        this.shop = shop;
        this.shop.balance = this.shop.balance + 100;

    }

    public void transmuter_Relic(RelicItem relic, Shop shop) {
        this.relic = relic;
        this.relic.quality -= 10;
        if(this.relic.quality < 0){
            this.relic.quality = 0;
        }

        this.relic = relic;
        this.shop.balance = this.shop.balance + 100;

    }





}

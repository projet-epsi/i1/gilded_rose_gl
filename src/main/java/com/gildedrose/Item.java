package com.gildedrose;

public abstract class Item {

    public String name;
    public Integer sellIn;
    public double quality;
    public float value;

    public Item(String name, Integer sellIn, Integer quality,float value) {
        this.name = name;
        this.sellIn = sellIn;
        this.quality = quality;
        this.value = value;
    }

    public abstract void updateQuality();

    protected void ceilQualityToFifty() {
        this.quality = this.quality > 50 ? 50 : this.quality;
    }

    protected void floorQualityToZero() {
        this.quality = this.quality < 0 ? 0 : this.quality;
    }



    @Override
    public String toString() {
        return "Item{" +
            "name='" + name + '\'' +
            ", sellIn=" + sellIn +
            ", quality=" + quality +
            '}';
    }
}

package com.gildedrose;

import com.gildedrose.interfaces.InventoryRepository;
import com.gildedrose.items.*;

import java.io.*;
import java.util.ArrayList;
import java.util.List;

public class InMemoryInventoryRepository implements InventoryRepository {

    private List<Item> items;

    public InMemoryInventoryRepository() throws FileNotFoundException {
        this.items = new ArrayList<>();
        items.add(new GenericItem("Rose doréé classique",15,18,42.3F));
        items.add(new GenericItem("Rose doréé 0",15,0,55));
        items.add(new AgedItem("Aged Brie",5,15, 25));
        items.add(new AgedItem("Aged Brie 50",5,50,25.5F));
        items.add(new LegendaryItem("Sulfuras",8,80,78));
        items.add(new BackStageItem("Backstage passes",6,16,30));
        items.add(new ConjuredItem("Conjured product",15,30,33));
        items.add(new RelicItem("Relic item",null,10,10));

    }




    @Override
    public List<Item> getInventory() {
        return this.items;
    }

    @Override
    public void saveInventory(List<Item> items) {
        this.items = items;
    }
}
